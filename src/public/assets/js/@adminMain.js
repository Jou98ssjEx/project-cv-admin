/*==================== MENU SHOW Y HIDDEN ====================*/

// const showMenu = (toggleId, navId) => {
//     const toggle = document.getElementById(toggleId),
//         nav = document.getElementById(navId),
//         header = document.getElementById('header'),
//         main = document.getElementById('main');

//     // Validate the variable exists
//     if (toggle && nav) {
//         toggle.addEventListener('click', () => {
//             // We add the show-menu class to the div tag with the nav__menu class
//             nav.classList.toggle('show-menu');
//             header.classList.toggle('body-container');
//             // header.classList.toggle('contenidoT');
//             console.log('Aqui');
//         });
//     }
// }

// showMenu('menu-toggle', 'sidebar');

const showMenu = (toggleId, navId) => {
    const toggle = document.getElementById(toggleId),
        nav = document.getElementById(navId),
        header = document.getElementById('header'),
        main = document.getElementById('main'),
        footer = document.getElementById('footer');

    let active = true;

    if (toggle) {
        toggle.addEventListener('click', () => {
            if (active == true) {
                nav.classList.remove('hidden-menu');
                header.classList.remove('body-container-nav');
                main.classList.remove('body-container-nav');
                footer.classList.remove('body-container-nav');

                nav.classList.add('show-menu');
                header.classList.add('body-container');
                main.classList.add('body-container');
                footer.classList.add('body-container');
                active = false;
            } else {
                nav.classList.remove('show-menu');
                header.classList.remove('body-container');
                main.classList.remove('body-container');
                footer.classList.remove('body-container');

                nav.classList.add('hidden-menu');
                header.classList.add('body-container-nav');
                main.classList.add('body-container-nav');
                footer.classList.add('body-container-nav');
                active = true;

            }
        });
    }

    // Validate the variable exists
    // if (toggle && nav) {
    //     toggle.addEventListener('click', () => {
    //         // We add the show-menu class to the div tag with the nav__menu class
    //         nav.classList.toggle('show-menu');
    //         header.classList.toggle('body-container');
    //         // header.classList.toggle('contenidoT');
    //         console.log('Aqui');
    //     });
    // }
}

showMenu('menu-toggle', 'sidebar');

// const showMenu = (toggleId, sidebarId, mainContenId) => {
//     const toggle = document.getElementById(toggleId),
//         sidebar = document.getElementById(sidebarId),
//         mainContent = document.getElementById(mainContenId);
//     let active = true;

//     if (toggle) {
//         toggle.addEventListener('click', () => {
//             if (active == true) {
//                 sidebar.classList.add('show-menu');
//                 mainContent.classList.add('body-container');
//             } else {

//             }
//         });
//     }

// }

// showMenu('menu-toggle', 'sidebar', 'main-content');