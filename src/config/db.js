const mongoose = require('mongoose');

require('./config');

mongoose.connect(process.env.urlDB, {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    })
    .then(db => console.log('DB conect'))
    .catch(err => console.error(err));